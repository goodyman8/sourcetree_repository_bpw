﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class BeginGame : MonoBehaviour
{
    bool gameHasStarted = false;
    public Transform StartingCamera;
    private float startDelay = 1.5f;

    public void beginGame()
    {
        if (gameHasStarted == false)
        {
            gameHasStarted = true;
            Invoke("StartGame", startDelay);
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(0);
    }

    public void quit()
    {
        Application.Quit();
    }
}
