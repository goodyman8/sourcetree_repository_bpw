﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    static public float health;
    public Collider player;
    public float immunityTimer;
    private float oldImmunityTimer;

    // Start is called before the first frame update
    void Start()
    {
        health = 3;
        oldImmunityTimer = immunityTimer;
    }

    // Update is called once per frame
    void Update()
    {
        immunityTimer -= 1;
    }

    private void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Enemy" && immunityTimer <= 0) // If  you colide
        {
            health -= 1;
            immunityTimer = oldImmunityTimer;
        }
        if (health <= 0) //Disables movement and sets in endGame, resets level after a timer.
        {
            GetComponent<PlayerMovement>().enabled = false;
            GetComponent<CameraAim>().enabled = false;
            Debug.Log("0 health");
            FindObjectOfType<GameManager>().endGame();
        }
        // Health pickup collision
        if (health < 3 && collisionInfo.gameObject.tag == "Health")
        {
            Destroy(collisionInfo.gameObject);
            health += 1;
        }

        // Game end
        if (collisionInfo.gameObject.name == "LevelExit")
        {
            FindObjectOfType<GameManager>().finishGame();
        }

    }
}
