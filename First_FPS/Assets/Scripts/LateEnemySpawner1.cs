﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LateEnemySpawner1 : MonoBehaviour
{
    public GameObject Enemy;
    public Transform spawner;
    public float respawnTimer;
    private float oldTimer = 0;

    // Start is called before the first frame update
    void Start()
    {
        oldTimer = respawnTimer; //Onthoud eerste timer
        SpawnEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        if (respawnTimer > 0 && Score.score > 100) // Als timer boven 1 is
        {
            respawnTimer -= 1; // -1 (60 is 1 second)
        }
        if (respawnTimer == 0)
        {
            SpawnEnemy();
            if (Score.score < 150)
            {
                respawnTimer = oldTimer; // Pakt timer dat in begin is ingesteld.
            }
            else if (Score.score > 150 && Score.score < 200)
            {
                respawnTimer = oldTimer * 0.5f;
            }
            else if (Score.score > 200)
            {
                respawnTimer = oldTimer * 0.25f;
            }
        }
    }

    void SpawnEnemy()
    {
        Instantiate(Enemy, spawner.transform.position, spawner.transform.rotation); //Spawnt enemy
    }
}
