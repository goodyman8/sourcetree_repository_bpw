﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject Enemy;
    public Transform spawner;
    public float respawnTimer;
    private float oldTimer = 0;
    public float spawnAfter;
    public float stopAfter; // Stop na bepaalde tijd

    // Start is called before the first frame update
    void Start()
    {
        oldTimer = respawnTimer; //Onthoud eerste timer
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnAfter < 0)
        { 
           if (respawnTimer > 0) // Als timer boven 1 is
            {
                respawnTimer -= 1; // -1 (60 is 1 second)
            }
           if (respawnTimer == 0)
            {
                SpawnEnemy();             
                respawnTimer = oldTimer; // Pakt timer dat in begin is ingesteld.
                /* Score based respawn, not used anymore
                else if (Score.score > 100 && Score.score < 200)
                {
                    respawnTimer = oldTimer * 0.5f;
                }
                else if (Score.score > 200)
                {
                    respawnTimer = oldTimer * 0.25f;
                }
                */
            }
        }
        else
        {
            spawnAfter -= 1*Time.deltaTime;
        }
        if (stopAfter < 0)
        {
            Destroy(gameObject);
        }
        else
        {
            stopAfter -= 1 * Time.deltaTime;
        }
    }

    void SpawnEnemy()
    {
        Instantiate(Enemy,spawner.transform.position,spawner.transform.rotation); //Spawnt enemy
    }
}
