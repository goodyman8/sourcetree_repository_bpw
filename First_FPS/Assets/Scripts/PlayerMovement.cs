﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb; // References rigidbody (gonna be player)

    [Range(0,2000)]
    public float speed; // Speed variable used for calculating movement force

    // Start is called before the first frame update
    void Start()
    {    
    }

    // Update is called once per frame
    void Update()
    {
        Movement(); // Runs movement() script.
    }

    private void Movement()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        rb.MovePosition(transform.localPosition + (transform.forward * vertical + transform.right * horizontal).normalized * speed *Time.fixedDeltaTime); //moves rigidbody based on local horizontal and vertical axis, does this by changing the "transform" of the rigid body.

    }
}
