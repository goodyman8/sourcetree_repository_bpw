﻿using UnityEngine;

public class WeaponSwitching : MonoBehaviour // Tutorial from Brackeys
{
    public int selectedWeapon = 0; // What weapon do we currently have selected
    public bool SmgAkimbo = false;
    public bool Shotgun = false;

    // Start is called before the first frame update
    void Start()
    {
        SelectWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        int previousSelectedWeapon = selectedWeapon;

        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (selectedWeapon >= transform.childCount - 1) // If selected weapon number from scrolling > more than child
                selectedWeapon = 0;
            else
                selectedWeapon++; // Go up in index
        }

        /*
        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (selectedWeapon <= 0) // If selected weapon number from scrolling < 0
                selectedWeapon = 0;
            else
                selectedWeapon--; // Go down in index
        }
        */

        if (Input.GetKeyDown(KeyCode.Alpha1)) // Changes weapon if you press 1, repeated for other functions
        {
            selectedWeapon = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && transform.childCount >= 2 && Shotgun == true) // Changes weapon if you press 1, repeated for other functions
        {
            selectedWeapon = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && transform.childCount >= 3 && SmgAkimbo == true) // Changes weapon if you press 1, repeated for other functions
        {
            selectedWeapon = 2;
        }

        if (previousSelectedWeapon != selectedWeapon) // If previous weapon index isn't equal to the current weapon
        {
            SelectWeapon(); // Run select weapon script, changing the current weapon to match index.
        }
    }


    void SelectWeapon()
    {
        int i = 0; // index numbers
        foreach (Transform weapon in transform) // for each transform in transform (all childs of current transform weaponholder) referred to current one as weapon.
        {
            if (i == selectedWeapon) // If indexes match
                weapon.gameObject.SetActive(true); // Activate game object
            else // Otherwise
                weapon.gameObject.SetActive(false); // Disable game object
            i++; // Adds index numbers to each child
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
       if(collision.gameObject.name == "SmgPickup")
        {
            Destroy(collision.gameObject);
            SmgAkimbo = true;
        }
        if (collision.gameObject.name == "ShotgunPickup")
        {
            Destroy(collision.gameObject);
            Shotgun = true;
        }

    }
}
