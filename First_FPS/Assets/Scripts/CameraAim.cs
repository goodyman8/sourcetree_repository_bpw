﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraAim : MonoBehaviour
{
    [SerializeField]
    private PlayerMovement player;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X"); //get mouse X position 
        float mouseY = Input.GetAxis("Mouse Y"); //get mouse Y position

        transform.eulerAngles += new Vector3(-mouseY, mouseX,0); // Euler angles (x,y,z)
        //player.transform.LookAt(this.transform.forward);
        player.transform.eulerAngles += new Vector3(0, mouseX, 0); // Removed Y because model clips through ground.
    }
}
