﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; // Needed to use unity AI

public class EnemyController : MonoBehaviour
{
    public bool enemyDead = false;
    public float lookRadius = 10f;
    public float health;
    Transform target; // Assign target (player)
    NavMeshAgent agent; // Assign NavMesh
    public Collider enemyCollider;
    Animator anim; // Animator object


    // Start is called before the first frame update
    void Start()
    {
        target = PlayerManager.instance.player.transform; //Makes sure to constantly target the instance player in transform.
        agent = GetComponent<NavMeshAgent>(); // Assign enemy to this(?)
        anim = GetComponent<Animator>(); // Assign animator to this
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);

        if (distance <= lookRadius && enemyDead == false)
        {
            agent.SetDestination(target.position); // If player is within look radius, informs enemy of location
        }
        if (enemyDead == true) // If enemy is dead = Rotate, scale down and destroy after timer. for dramatic effect.
        {
            enemyCollider.enabled = false;
            gameObject.GetComponent<NavMeshAgent>().enabled = false; // Stop chasing player, disable navmesh
            gameObject.transform.Rotate(0, 25, 0); // Rotate object
            gameObject.transform.localScale -= new Vector3(0.6f,0.6f,0.6f)*Time.deltaTime;
            Destroy(gameObject, 2f);
        }
    }

    private void OnDrawGizmosSelected() // Draws "Gizmo", in this case look radius
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius); // Draws radius from current position
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if(collisionInfo.collider.tag == "Player")
        {
            anim.SetBool("playerCollision", true);
        }
        if (collisionInfo.collider.tag == "Bullet") // Use tags to discern bullet type.
        {
            if (health == 0)
            {
                enemyDead = true;
            }
            else
            {
                health -= 1;
            }
        }
    }

    private void OnCollisionExit(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Player")
        {
            anim.SetBool("playerCollision", false);
        }
    }
}
