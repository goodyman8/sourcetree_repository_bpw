﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderScript : MonoBehaviour
{
    public float disappearTimer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (disappearTimer < 0)
        {
            gameObject.transform.localPosition -= new Vector3(0f, 2f, 0f) * Time.deltaTime;
            Destroy(gameObject,3);

        }
        else
        {
            disappearTimer -= 1 * Time.deltaTime;
        }
    }
}
