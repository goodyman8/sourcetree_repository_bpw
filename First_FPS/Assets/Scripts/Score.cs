﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    //** Score, overbodig
    public static float score; // Met static kan ik score ook op andere plekken gebruiken.
    public Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        score += 10*Time.deltaTime;
        scoreText.text = score.ToString("0");
    }
}
