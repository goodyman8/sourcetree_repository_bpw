﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunAnimation : MonoBehaviour
{
    public Animator anim;
    public float reload;
    private float reloadStart;

    // Start is called before the first frame update
    void Start()
    {
        reloadStart = reload;
        anim = GetComponent<Animator>(); // Assign animator to this
        reload = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && reload <= 0)
        {
            reload = reloadStart;
            anim.SetBool("Shoot", true);
        }
        if (reload > 0)
        {
            reload -= 1 * Time.deltaTime;
        }
        if (reload <= 0)
        {
            anim.SetBool("Shoot", false);
        }
    }
}
