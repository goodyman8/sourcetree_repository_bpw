﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{

    void Start()
    {
        Destroy(gameObject, 1); // Destroy projectile over 5 seconds.
    }

    private void Update()
    {
        gameObject.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f) * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Terrain" || collision.collider.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }
}