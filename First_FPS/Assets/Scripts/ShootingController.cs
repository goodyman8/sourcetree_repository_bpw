﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingController : MonoBehaviour // Tutorial by Firemind, Pellet count & Spreadangle tutorial by Mr Adhesive (code marked with ** for second tutorial)
{
    // Unity projectile based shooting from tutorial
    public float moveForce; // force used to move bullet
    public GameObject bullet; // Takes game object bullet
    public Transform gun; // Takes "transform" from gun instance
    Animator anim;

    // Extra gun options
    public float shootRate; // Fire rate in seconds.
    public float shootForce; // Force added to shoot.
    public float spreadAngle; // Angle of gun spread **

    public int pelletCount; // Amount of pellets shot out**

    private float m_shootRateTimeStamp; // ??

    Animator m_Animator; // ??

    List<Quaternion> pellets; // ??**

    void Awake() // Awake is called once in its lifetime, after all calculations unlike start ** Only used for shotguns.
    {
        pellets = new List<Quaternion>(pelletCount);
        for (int i = 0; i < pelletCount; i++) // If index below assigned pellet count, add index and **
        {
            pellets.Add(Quaternion.Euler(Vector3.zero)); //add pellet with empty vector 3 **
        }
    }

    void Update()
    {

        if (Input.GetMouseButton(0)) //If you hold left mouse button.
        {
            
            /* Old script, still using the fire rate from this, but new one allows pellets & spread angle.
            if (Time.time > m_shootRateTimeStamp) // If player is allowed to shoot
            {
                GameObject go = (GameObject)Instantiate(
                bullet, gun.position, gun.rotation); //Creates game object with instantiate

                go.GetComponent<Rigidbody>().AddForce(gun.forward * shootForce); // adds the shootforce to the bullet in the forward axis of the world(add Y axis as well). 
                m_shootRateTimeStamp = Time.time + shootRate; // adds time to shoot rate?
            }
            */

            if (Time.time > m_shootRateTimeStamp)
            {
                fire();
            }
        }

    }
   
    void fire() //test**
    {
        for (int i = 0; i < pelletCount; i++) // for loop, checks pellet count and adds to it if its smaller than pellet count.
        {
            pellets[i] = Random.rotation; // Returns a random rotation to indexed pallets.

            GameObject p  = Instantiate(bullet, gun.position,gun.rotation); // Instantiates bullets in the gun position and rotation.
            p.transform.rotation = Quaternion.RotateTowards(p.transform.rotation, pellets[i], spreadAngle); // Rotates bullets towards 
            p.GetComponent<Rigidbody>().AddForce(p.transform.forward * shootForce);
            i ++;

            m_shootRateTimeStamp = Time.time + shootRate; // Resets shooting timer
        }
    }

}